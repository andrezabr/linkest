<?php
class AppSchema extends CakeSchema {

	public function before($event = array()) {
		return true;
	}

	public function after($event = array()) {
	}

	public $categories = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'parent_id'       => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => '4'),
		'title'           => array('type' => 'string', 'null' => false, 'default' => null),
		'slug'            => array('type' => 'string', 'null' => false, 'default' => null),
		'description'     => array('type' => 'text', 'null' => true, 'default' => null, 'length' => '16'),
		'lft'             => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => '4'),
		'rght'            => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => '4'),
		'link_count'      => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => '4'),
		'created'         => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified'        => array('type' => 'datetime', 'null' => true, 'default' => null),
		'is_editable'     => array('type' => 'boolean', 'null' => true, 'default' => '1', 'length' => '1'),
		'deleted'         => array('type' => 'boolean', 'null' => false, 'default' => '0', 'length' => '1'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $favorites = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'link_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $flags = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'link_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'created'         => array('type' => 'datetime', 'null' => false, 'default' => null),
		'ip'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $follows = array(
		'id'                => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'following_user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'user_id'           => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'           => array(),
		'tableParameters'   => array()
	);

	public $groups = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'titulo'          => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '60'),
		'description'     => array('type' => 'string', 'null' => true, 'default' => null),
		'created'         => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified'        => array('type' => 'string', 'null' => true, 'default' => null, 'length' => '45'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $link_category = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'category_id'     => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'link_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $links = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'url'             => array('type' => 'string', 'null' => false, 'default' => null),
		'ip'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'machine'         => array('type' => 'string', 'null' => false, 'default' => null),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $members = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'group_id'        => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $preferences = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'category_id'     => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'user_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $reviews = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'user_id'         => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => '4'),
		'link_id'         => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => '4'),
		'r_like'          => array('type' => 'boolean', 'null' => true, 'default' => '0', 'length' => '1'),
		'r_dislike'       => array('type' => 'boolean', 'null' => true, 'default' => '0', 'length' => '1'),
		'created'         => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $roles = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'title'           => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '45'),
		'slug'            => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '25'),
		'indexes'         => array(),
		'tableParameters' => array()
	);

	public $users = array(
		'id'              => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'role_id'         => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => '4'),
		'username'        => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '16'),
		'email'           => array('type' => 'string', 'null' => false, 'default' => null),
		'password'        => array('type' => 'string', 'null' => true, 'default' => null, 'length' => '32'),
		'first_name'      => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '45'),
		'last_name'       => array('type' => 'string', 'null' => false, 'default' => null, 'length' => '60'),
		'activated'       => array('type' => 'boolean', 'null' => true, 'default' => '1', 'length' => '1'),
		'deleted'         => array('type' => 'boolean', 'null' => true, 'default' => '0', 'length' => '1'),
		'created'         => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified'        => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes'         => array(),
		'tableParameters' => array()
	);

}
