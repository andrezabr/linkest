USE [linkest]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__link___2D27B809]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__link___2D27B809]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] DROP CONSTRAINT [DF__categorie__link___2D27B809]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__is_ed__2E1BDC42]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__is_ed__2E1BDC42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] DROP CONSTRAINT [DF__categorie__is_ed__2E1BDC42]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__delet__2F10007B]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__delet__2F10007B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] DROP CONSTRAINT [DF__categorie__delet__2F10007B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__reviews__r_like__412EB0B6]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__reviews__r_like__412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reviews] DROP CONSTRAINT [DF__reviews__r_like__412EB0B6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__reviews__r_disli__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__reviews__r_disli__4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reviews] DROP CONSTRAINT [DF__reviews__r_disli__4222D4EF]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__users__activated__33D4B598]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users__activated__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users] DROP CONSTRAINT [DF__users__activated__33D4B598]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__users__deleted__34C8D9D1]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users__deleted__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users] DROP CONSTRAINT [DF__users__deleted__34C8D9D1]
END


End
GO
/****** Object:  ForeignKey [FK_categories_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_categories_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
ALTER TABLE [dbo].[categories] DROP CONSTRAINT [FK_categories_0]
GO
/****** Object:  ForeignKey [FK_favorites_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites] DROP CONSTRAINT [FK_favorites_0]
GO
/****** Object:  ForeignKey [FK_favorites_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites] DROP CONSTRAINT [FK_favorites_1]
GO
/****** Object:  ForeignKey [FK_flags_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags] DROP CONSTRAINT [FK_flags_0]
GO
/****** Object:  ForeignKey [FK_flags_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags] DROP CONSTRAINT [FK_flags_1]
GO
/****** Object:  ForeignKey [FK_follows_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows] DROP CONSTRAINT [FK_follows_0]
GO
/****** Object:  ForeignKey [FK_follows_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows] DROP CONSTRAINT [FK_follows_1]
GO
/****** Object:  ForeignKey [FK_groups_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_groups_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[groups]'))
ALTER TABLE [dbo].[groups] DROP CONSTRAINT [FK_groups_0]
GO
/****** Object:  ForeignKey [FK_link_category_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category] DROP CONSTRAINT [FK_link_category_0]
GO
/****** Object:  ForeignKey [FK_link_category_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category] DROP CONSTRAINT [FK_link_category_1]
GO
/****** Object:  ForeignKey [FK_links_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_links_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[links]'))
ALTER TABLE [dbo].[links] DROP CONSTRAINT [FK_links_0]
GO
/****** Object:  ForeignKey [FK_members_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members] DROP CONSTRAINT [FK_members_0]
GO
/****** Object:  ForeignKey [FK_members_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members] DROP CONSTRAINT [FK_members_1]
GO
/****** Object:  ForeignKey [FK_preferences_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences] DROP CONSTRAINT [FK_preferences_0]
GO
/****** Object:  ForeignKey [FK_preferences_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences] DROP CONSTRAINT [FK_preferences_1]
GO
/****** Object:  ForeignKey [FK_reviews_0]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews] DROP CONSTRAINT [FK_reviews_0]
GO
/****** Object:  ForeignKey [FK_reviews_1]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews] DROP CONSTRAINT [FK_reviews_1]
GO
/****** Object:  ForeignKey [FK_users_0]    Script Date: 06/05/2014 12:25:48 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_users_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
ALTER TABLE [dbo].[users] DROP CONSTRAINT [FK_users_0]
GO
/****** Object:  Table [dbo].[favorites]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[favorites]') AND type in (N'U'))
DROP TABLE [dbo].[favorites]
GO
/****** Object:  Table [dbo].[flags]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[flags]') AND type in (N'U'))
DROP TABLE [dbo].[flags]
GO
/****** Object:  Table [dbo].[link_category]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[link_category]') AND type in (N'U'))
DROP TABLE [dbo].[link_category]
GO
/****** Object:  Table [dbo].[members]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[members]') AND type in (N'U'))
DROP TABLE [dbo].[members]
GO
/****** Object:  Table [dbo].[reviews]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reviews]') AND type in (N'U'))
DROP TABLE [dbo].[reviews]
GO
/****** Object:  Table [dbo].[preferences]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[preferences]') AND type in (N'U'))
DROP TABLE [dbo].[preferences]
GO
/****** Object:  Table [dbo].[links]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[links]') AND type in (N'U'))
DROP TABLE [dbo].[links]
GO
/****** Object:  Table [dbo].[follows]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[follows]') AND type in (N'U'))
DROP TABLE [dbo].[follows]
GO
/****** Object:  Table [dbo].[groups]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[groups]') AND type in (N'U'))
DROP TABLE [dbo].[groups]
GO
/****** Object:  Table [dbo].[users]    Script Date: 06/05/2014 12:25:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type in (N'U'))
DROP TABLE [dbo].[users]
GO
/****** Object:  Table [dbo].[categories]    Script Date: 06/05/2014 12:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[categories]') AND type in (N'U'))
DROP TABLE [dbo].[categories]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 06/05/2014 12:25:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[roles]') AND type in (N'U'))
DROP TABLE [dbo].[roles]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 06/05/2014 12:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](45) NOT NULL,
	[slug] [varchar](25) NOT NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [unique_title_role] UNIQUE NONCLUSTERED 
(
	[title] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[categories]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[categories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[title] [varchar](255) NOT NULL,
	[slug] [varchar](255) NOT NULL,
	[description] [text] NULL,
	[lft] [int] NULL,
	[rght] [int] NULL,
	[link_count] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NULL,
	[is_editable] [bit] NULL,
	[deleted] [bit] NOT NULL,
 CONSTRAINT [PK_categories] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [unique_title_category] UNIQUE NONCLUSTERED 
(
	[title] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 06/05/2014 12:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[role_id] [int] NOT NULL,
	[username] [varchar](16) NOT NULL,
	[email] [varchar](255) NOT NULL,
	[password] [varchar](32) NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](60) NOT NULL,
	[activated] [bit] NULL,
	[deleted] [bit] NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [unique_email_role] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[groups]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[groups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[titulo] [varchar](60) NOT NULL,
	[description] [varchar](255) NULL,
	[created] [datetime] NOT NULL,
	[modified] [varchar](45) NULL,
 CONSTRAINT [PK_groups] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[follows]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[follows]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[follows](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[following_user_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_follows] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[links]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[links]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[links](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[url] [varchar](255) NOT NULL,
	[ip] [int] NOT NULL,
	[machine] [varchar](255) NOT NULL,
 CONSTRAINT [PK_links] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[preferences]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[preferences]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[preferences](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_preferences] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[reviews]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reviews]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[reviews](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[link_id] [int] NULL,
	[r_like] [bit] NULL,
	[r_dislike] [bit] NULL,
	[created] [datetime] NOT NULL,
 CONSTRAINT [PK_reviews] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[members]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[members]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[members](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
 CONSTRAINT [PK_members] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[link_category]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[link_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[link_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NOT NULL,
	[link_id] [int] NOT NULL,
 CONSTRAINT [PK_link_category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[flags]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[flags]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[flags](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[link_id] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[ip] [int] NOT NULL,
 CONSTRAINT [PK_flags] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[favorites]    Script Date: 06/05/2014 12:25:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[favorites]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[favorites](
	[id] [int] NOT NULL,
	[link_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_favorites] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Default [DF__categorie__link___2D27B809]    Script Date: 06/05/2014 12:25:47 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__link___2D27B809]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__link___2D27B809]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] ADD  CONSTRAINT [DF__categorie__link___2D27B809]  DEFAULT ((0)) FOR [link_count]
END


End
GO
/****** Object:  Default [DF__categorie__is_ed__2E1BDC42]    Script Date: 06/05/2014 12:25:47 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__is_ed__2E1BDC42]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__is_ed__2E1BDC42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] ADD  CONSTRAINT [DF__categorie__is_ed__2E1BDC42]  DEFAULT ((1)) FOR [is_editable]
END


End
GO
/****** Object:  Default [DF__categorie__delet__2F10007B]    Script Date: 06/05/2014 12:25:47 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__categorie__delet__2F10007B]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__categorie__delet__2F10007B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[categories] ADD  CONSTRAINT [DF__categorie__delet__2F10007B]  DEFAULT ((0)) FOR [deleted]
END


End
GO
/****** Object:  Default [DF__reviews__r_like__412EB0B6]    Script Date: 06/05/2014 12:25:47 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__reviews__r_like__412EB0B6]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__reviews__r_like__412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reviews] ADD  CONSTRAINT [DF__reviews__r_like__412EB0B6]  DEFAULT ('0') FOR [r_like]
END


End
GO
/****** Object:  Default [DF__reviews__r_disli__4222D4EF]    Script Date: 06/05/2014 12:25:48 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__reviews__r_disli__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__reviews__r_disli__4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reviews] ADD  CONSTRAINT [DF__reviews__r_disli__4222D4EF]  DEFAULT ('0') FOR [r_dislike]
END


End
GO
/****** Object:  Default [DF__users__activated__33D4B598]    Script Date: 06/05/2014 12:25:48 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__users__activated__33D4B598]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users__activated__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__activated__33D4B598]  DEFAULT ((1)) FOR [activated]
END


End
GO
/****** Object:  Default [DF__users__deleted__34C8D9D1]    Script Date: 06/05/2014 12:25:48 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__users__deleted__34C8D9D1]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users__deleted__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__deleted__34C8D9D1]  DEFAULT ((0)) FOR [deleted]
END


End
GO
/****** Object:  ForeignKey [FK_categories_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_categories_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
ALTER TABLE [dbo].[categories]  WITH CHECK ADD  CONSTRAINT [FK_categories_0] FOREIGN KEY([parent_id])
REFERENCES [dbo].[categories] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_categories_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[categories]'))
ALTER TABLE [dbo].[categories] CHECK CONSTRAINT [FK_categories_0]
GO
/****** Object:  ForeignKey [FK_favorites_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites]  WITH CHECK ADD  CONSTRAINT [FK_favorites_0] FOREIGN KEY([link_id])
REFERENCES [dbo].[links] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites] CHECK CONSTRAINT [FK_favorites_0]
GO
/****** Object:  ForeignKey [FK_favorites_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites]  WITH CHECK ADD  CONSTRAINT [FK_favorites_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_favorites_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[favorites]'))
ALTER TABLE [dbo].[favorites] CHECK CONSTRAINT [FK_favorites_1]
GO
/****** Object:  ForeignKey [FK_flags_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags]  WITH CHECK ADD  CONSTRAINT [FK_flags_0] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags] CHECK CONSTRAINT [FK_flags_0]
GO
/****** Object:  ForeignKey [FK_flags_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags]  WITH CHECK ADD  CONSTRAINT [FK_flags_1] FOREIGN KEY([link_id])
REFERENCES [dbo].[links] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_flags_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[flags]'))
ALTER TABLE [dbo].[flags] CHECK CONSTRAINT [FK_flags_1]
GO
/****** Object:  ForeignKey [FK_follows_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows]  WITH CHECK ADD  CONSTRAINT [FK_follows_0] FOREIGN KEY([following_user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows] CHECK CONSTRAINT [FK_follows_0]
GO
/****** Object:  ForeignKey [FK_follows_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows]  WITH CHECK ADD  CONSTRAINT [FK_follows_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_follows_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[follows]'))
ALTER TABLE [dbo].[follows] CHECK CONSTRAINT [FK_follows_1]
GO
/****** Object:  ForeignKey [FK_groups_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_groups_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[groups]'))
ALTER TABLE [dbo].[groups]  WITH CHECK ADD  CONSTRAINT [FK_groups_0] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_groups_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[groups]'))
ALTER TABLE [dbo].[groups] CHECK CONSTRAINT [FK_groups_0]
GO
/****** Object:  ForeignKey [FK_link_category_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category]  WITH CHECK ADD  CONSTRAINT [FK_link_category_0] FOREIGN KEY([category_id])
REFERENCES [dbo].[categories] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category] CHECK CONSTRAINT [FK_link_category_0]
GO
/****** Object:  ForeignKey [FK_link_category_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category]  WITH CHECK ADD  CONSTRAINT [FK_link_category_1] FOREIGN KEY([link_id])
REFERENCES [dbo].[links] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_link_category_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[link_category]'))
ALTER TABLE [dbo].[link_category] CHECK CONSTRAINT [FK_link_category_1]
GO
/****** Object:  ForeignKey [FK_links_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_links_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[links]'))
ALTER TABLE [dbo].[links]  WITH CHECK ADD  CONSTRAINT [FK_links_0] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_links_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[links]'))
ALTER TABLE [dbo].[links] CHECK CONSTRAINT [FK_links_0]
GO
/****** Object:  ForeignKey [FK_members_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members]  WITH CHECK ADD  CONSTRAINT [FK_members_0] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members] CHECK CONSTRAINT [FK_members_0]
GO
/****** Object:  ForeignKey [FK_members_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members]  WITH CHECK ADD  CONSTRAINT [FK_members_1] FOREIGN KEY([group_id])
REFERENCES [dbo].[groups] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_members_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[members]'))
ALTER TABLE [dbo].[members] CHECK CONSTRAINT [FK_members_1]
GO
/****** Object:  ForeignKey [FK_preferences_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences]  WITH CHECK ADD  CONSTRAINT [FK_preferences_0] FOREIGN KEY([category_id])
REFERENCES [dbo].[categories] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences] CHECK CONSTRAINT [FK_preferences_0]
GO
/****** Object:  ForeignKey [FK_preferences_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences]  WITH CHECK ADD  CONSTRAINT [FK_preferences_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_preferences_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[preferences]'))
ALTER TABLE [dbo].[preferences] CHECK CONSTRAINT [FK_preferences_1]
GO
/****** Object:  ForeignKey [FK_reviews_0]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews]  WITH CHECK ADD  CONSTRAINT [FK_reviews_0] FOREIGN KEY([link_id])
REFERENCES [dbo].[links] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews] CHECK CONSTRAINT [FK_reviews_0]
GO
/****** Object:  ForeignKey [FK_reviews_1]    Script Date: 06/05/2014 12:25:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews]  WITH CHECK ADD  CONSTRAINT [FK_reviews_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_reviews_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[reviews]'))
ALTER TABLE [dbo].[reviews] CHECK CONSTRAINT [FK_reviews_1]
GO
/****** Object:  ForeignKey [FK_users_0]    Script Date: 06/05/2014 12:25:48 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_users_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_0] FOREIGN KEY([role_id])
REFERENCES [dbo].[roles] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_users_0]') AND parent_object_id = OBJECT_ID(N'[dbo].[users]'))
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_0]
GO
