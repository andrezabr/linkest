// Custom Paths
var CKEDITOR_CONFIG = WEBROOT + 'PoloCms/js/app/vendor/ckeditor/ckeditor.config.js';


//The build will inline common dependencies into this file.
requirejs.config({
    baseUrl: WEBROOT + 'PoloCms/js/',
    //enforceDefine: true, //To get timely, correct error triggers in IE, force a define/shim exports check.
    paths: {
        'jquery': 'vendor/jquery.min',
        // 'jquery': [
            // 'http://code.jquery.com/jquery-1.9.1.min',
            //If the CDN location fails, load from this location
            // 'vendor/jquery.min'
        // ],
        'main'                 : 'admin/main',
        'async'                : 'vendor/requirejs/async',
        'classie'              : 'vendor/classie',
        'ckeditor'             : 'vendor/ckeditor/ckeditor',
        'echo'                 : 'vendor/echo',
        'mlpushmenu'           : 'vendor/mlpushmenu',
        'nicescroll'           : 'vendor/nicescroll/jquery.nicescroll.min',
        'fancybox'             : 'vendor/fancybox/jquery.fancybox',
        'bootstrap-switch'     : 'vendor/bootstrap-switch/bootstrap-switch.min',
        'hoverIntent'          : 'vendor/jquery.hoverIntent',
        'mask'                 : 'vendor/jquery.mask/jquery.mask.min',
        'pnotify'              : 'vendor/pnotify/jquery.pnotify',
        'typeahead'            : 'vendor/typeahead.bundle',
        'bootstrap-tagsinput'  : 'vendor/bootstrap-tagsinput/bootstrap-tagsinput',
        'bootstrap-tokenfield' : 'vendor/bootstrap-tokenfield/bootstrap-tokenfield.min',

        'utils'                : 'app/utils/functions',
        'MapaPreview'          : 'app/vendor/MapaPreview',

        // vendors
        'affix'      : 'vendor/bootstrap/affix',
        'alert'      : 'vendor/bootstrap/alert',
        'button'     : 'vendor/bootstrap/button',
        'carousel'   : 'vendor/bootstrap/carousel',
        'collapse'   : 'vendor/bootstrap/collapse',
        'dropdown'   : 'vendor/bootstrap/dropdown',
        'modal'      : 'vendor/bootstrap/modal',
        'popover'    : 'vendor/bootstrap/popover',
        'scrollspy'  : 'vendor/bootstrap/scrollspy',
        'tab'        : 'vendor/bootstrap/tab',
        'tooltip'    : 'vendor/bootstrap/tooltip',
        'transition' : 'vendor/bootstrap/transition',
        'bootstrap'  : 'vendor/bootstrap/bootstrap',

        // FileUpload
        'fileUploadConfig'           : 'app/fileUpload/fileUploadConfig',
        'singleImageUploadHandler'   : 'app/fileUpload/singleImageUploadHandler',
        'singleVideoUploadHandler'   : 'app/fileUpload/singleVideoUploadHandler',
        'multipleImageUploadHandler' : 'app/fileUpload/multipleImageUploadHandler',
        'multipleVideoUploadHandler' : 'app/fileUpload/multipleVideoUploadHandler',
        'multipleFileUploadHandler'  : 'app/fileUpload/multipleFileUploadHandler',

        // TODO:
        // 'multipleAudioUpload' : 'app/fileUpload/multipleAudioUpload',
        // 'multipleLinkUpload' : 'app/fileUpload/multipleLinkUpload',

        // FileUpload Dependencies
        'fileUploadDependencies'        : 'app/fileUpload/dependencies',
        'tmpl'                          : 'vendor/blueimp/templates/tmpl',
        'load-image'                    : 'vendor/blueimp/load-image/load-image',
        'jquery.ui.widget'              : 'vendor/blueimp/jquery-fileupload/vendor/jquery.ui.widget',
        'jquery.iframe-transport'       : 'vendor/blueimp/jquery-fileupload/jquery.iframe-transport',
        'jquery.fileupload'             : 'vendor/blueimp/jquery-fileupload/jquery.fileupload',
        'jquery.fileupload-process'     : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-process',
        'jquery.fileupload-image'       : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-image',
        'jquery.fileupload-ui'          : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-ui',
        // Other FileUpload Dependencies
        'jquery.postmessage-transport'  : 'vendor/blueimp/jquery-fileupload/cors/jquery.postmessage-transport',
        'jquery.fileupload-angular'     : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-angular',
        'jquery.fileupload-audio'       : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-audio',
        'jquery.fileupload-video'       : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-video',
        'jquery.fileupload-validate'    : 'vendor/blueimp/jquery-fileupload/jquery.fileupload-validate',
        //File uploader external dependencies...
        'load-image-meta'               : 'vendor/blueimp/load-image/load-image-meta',
        'load-image-exif'               : 'vendor/blueimp/load-image/load-image-exif',
        'load-image-ios'                : 'vendor/blueimp/load-image/load-image-ios',
        'canvas-to-blob'                : 'vendor/blueimp/canvas-to-blob/canvas-to-blob',

        // The XDomainRequest Transport is included for cross-domain file deletion for IE8+
        'xdr-domain'                    : 'vendor/blueimp/jquery-fileupload/cors/jquery.xdr-transport'
    },
    shim: {
        'mlpushmenu' : {
            deps    : ['classie'],
            exports : 'mlPushMenu'
        },
        'ckeditor' : {
            exports : 'CKEDITOR'
        },
        'echo'             : { exports : 'Echo' },
        'nicescroll'       : ['jquery'],
        'bootstrap-switch' : ['jquery'],
        'typeahead'        : ['jquery'],
        'mask'             : ['jquery'],
        'tmpl'             : ['jquery']
    }
});

// Load the main app module to start the app
requirejs(["main"]);