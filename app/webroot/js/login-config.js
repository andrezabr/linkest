//The build will inline common dependencies into this file.
requirejs.config({
    baseUrl: WEBROOT + 'PoloCms/js/',
    //enforceDefine: true, //To get timely, correct error triggers in IE, force a define/shim exports check.
    paths: {
        'jquery': 'vendor/jquery.min',
        // 'jquery': [
            // 'http://code.jquery.com/jquery-1.9.1.min',
            //If the CDN location fails, load from this location
            // 'vendor/jquery.min'
        // ],
        'main'                : 'admin/main',
        'bootstrap'           : 'vendor/bootstrap/bootstrap.min',
        'bootstrap-switch'    : 'vendor/bootstrap-switch/bootstrap-switch.min',
        'pnotify'             : 'vendor/pnotify/jquery.pnotify',
        'utils'               : 'app/utils/functions'
    },
    shim: {
        'bootstrap'  : ['jquery'],
        'bootstrap-switch' : ['jquery']
    }
});