/*!
* Admin
* admin/main.js
* Funções comuns a todos os arquivos do site admin
*/
define(function (require) {
    'use strict';

    var $          = require('jquery'),
        utils      = require('utils'),
        mlPushMenu = require('mlpushmenu'),
        Echo       = require('echo');

        require('bootstrap');
        require('nicescroll');
        require('fancybox');
        require('bootstrap-switch');
        require('pnotify');
        // require('hoverIntent');

    $(function() {
        /**
         * Ativa o push menu
         */
        new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ), {
            type : 'cover'
        } );

        $( 'a[href="#"]' ).click( function(e) {
            e.preventDefault();
        });

        // SVG fallback
        /*if(!Modernizr.svg) {
            $('img[src*="svg"]').attr('src', function() {
                return $(this).attr('src').replace('.svg', '.png');
            });
        }*/

        /* Tooltips */
        if( $.fn.tooltip ){
            $('[data-toggle^="tooltip"]').tooltip();
            $('.tooltip-alert').tooltip();
        }

        if($.fn.fancybox) {
            $(".fancybox").fancybox();

            $(".fancyBoxIframe").fancybox({
                'type'           : 'iframe',
                'width'          : 420,
                'height'         : 380,
                'autoScale'      : true,
                'autoDimensions' : false
            });
        }

        /* Chosen Select Box Plugin */
        /*if($.fn.chosen) {
            $('select.chosen-select').chosen();
        }*/

        /* Tooltips */
        if( $.fn.tooltip ){
            $('[data-toggle^="tooltip"]').tooltip();
            $('.tooltip-alert').tooltip();
        }

        /*if($.fn.spinner) {
            $( ".spinner" ).spinner();
            $( ".spinner_range" ).spinner({
                spin: function( event, ui ) {
                    if ( ui.value > 20 ) {
                        $( this ).spinner( "value", 0 );
                        return false;
                    } else if ( ui.value < 0 ) {
                        $( this ).spinner( "value", 20 );
                        return false;
                    }
                }
            });
        }*/

        if( $.fn.bootstrapSwitch ){
            $('.make-switch').bootstrapSwitch();
            $('.make-switch.iradio').on('switch-change', function () {
                $('.make-switch.iradio').bootstrapSwitch('toggleRadioStateAllowUncheck', false);
            });
        }

        //
        // Enable/Disable (ToggleStatus)
        //
        $(document).on('click', '.togglestatus-button', function(event) {
            event.preventDefault();
            var $this = $(this);

            if(confirm($this.data('confirm'))){
                var action = $this.data('toggle-action');

                $.post( $this.attr('href'), $this.serialize())
                    .done(function( response ) {
                        response = $.parseJSON(response);

                        if(response.status === "success"){
                            var toggleText = 'Habilitar';
                            if($this.text() === toggleText){
                                toggleText = 'Desabilitar';
                                $this.parents('tr').find('.label-danger').remove();
                            }else{
                                $this.parents('tr').find('.column-main').append(' <small class="label label-danger">inativo</small> ');
                            }

                            $this.text(toggleText);
                            $this.parents('tr').toggleClass('danger').toggleClass('error');
                        }else{
                            utils.alertError(response.message);
                        }
                    })
                    .fail(function( response ) {
                        utils.alertError( "Não foi possível " + action + " o registro.");
                        console.log(response);
                    });
            }
        });

        //
        // Delete single (in admin_index pages)
        //
        $(document).on('click', '.disableSingle-button, .deleteSingle-button', function(event) {
            event.preventDefault();
            var $this = $(this);

            if(confirm($this.data('confirm'))){

                var $btn = $this.parents('.btn-group').children('.dropdown-toggle');
                    $btn.button('excluindo');

                $.post( $this.attr('href'), $this.serialize())
                    .done(function( response ) {
                        response = $.parseJSON(response);
                        $btn.button('reset');
                        if(response.status === "success"){
                            $this.parents('tr').addClass('success').fadeOut('slow', function() {
                                $(this).remove();

                                var count = $this.parents('tbody').children('tr').length;
                                if(count == 0){
                                    $('#'+ MODEL +'AdminIndexForm').remove();
                                    $('#del-'+ MODEL).remove(); // botão de deletar mútiplos

                                    //
                                    // TODO: ler o HTML de um template
                                    //
                                    $('#PageContent').append('<div class="jumbotron text-center no-mb"> <p class="lead"><i class="icomoon icon-wondering2 icon-lg text-muted size-x3"></i></p> <p class="lead text-center no-mb">Nenhum conteúdo foi adicionado ainda.</p> </div>');
                                }
                            });
                        }else{
                            utils.alertError(response.message);
                        }
                    })
                    .fail(function( response ) {
                        utils.alertError( "Não foi possível excluir o registro.");
                        console.log(response);
                    });
            }
        });

        //
        // Delete Multiple (in admin_index pages)
        //
        $(document).on('click', '.disable-button', function(event) {
            event.preventDefault();
            var $this = $(this),
                $form = $('#'+MODEL+'AdminIndexForm');
                // $form = $this.parents('form');

                $this.button('loading');

            if(confirm($this.data('confirm'))){
                $.post( $form.attr('action'), $form.serialize())
                    .done(function( response ) {
                        response = $.parseJSON(response);
                        $this.button('reset');

                        if(response.status === "success"){
                            $.each($form.serializeArray(), function(index, obj) {
                                if(obj.value == "1"){
                                    $("input[name='"+ obj.name +"']").parents('tr').addClass('success').fadeOut('slow', function() {
                                        $(this).remove();
                                    });
                                }
                            });
                        }else{
                            utils.alertError(response.message);
                        }
                    })
                    .fail(function( response ) {
                        utils.alertError( "Não foi possível deletar o(s) registro(s).");
                        console.log(response);
                    });
            }
        });

        //
        // Modal Category
        //
        $(document).on('submit', '#'+MODEL+'categoryAdminAddForm', function(event) {
            event.preventDefault();

            var $this = $(this),
                btn   = $this.find('.btn-success');
                btn.button('loading');

            $.post( $this.attr('action'), $this.serialize())
                .done(function( response ) {
                    console.log(response);
                    response = $.parseJSON(response);

                    btn.button('reset');
                    if(response.status === "success"){
                        utils.alertSuccess("A categoria foi salva com sucesso");

                        var target = $($('#AddCategory').data('combo'));
                        target.append('<option value="'+response.message.id+'" selected>'+response.message.title+'</option>');
                        $this.parents('.modal').modal('hide');
                    }else{
                        utils.alertError(response.message);
                        btn.removeClass('btn-success').addClass('btn-danger');
                    }
                })
                .fail(function( response ) {
                    utils.alertError( "Não foi possível salvar");
                    btn.button('reset');
                    btn.removeClass('btn-success').addClass('btn-danger');
                    console.log(response);
                });
        });

        //
        // Filter Index
        //
        $(document).on('submit', '#'+MODEL+'AdminIndexForm', function(event) {
            event.preventDefault();

            var $this  = $(this),
                btn    = $this.find('.btn'),
                $input = $('#'+MODEL+'Search');

            if($input.val() == '')
                utils.alertError( "Informe uma palavra para filtrar os resultados");
            else{
                btn.button('loading');
                $.get( $this.attr('action'), $this.serialize())
                    .done(function( response ) {
                        $("#PageContent").html(response);
                        btn.button('reset');
                    })
                    .fail(function( response ) {
                        utils.alertError( "Não foi possível filtrar os resultados");
                        btn.button('reset');
                        btn.removeClass('btn-success').addClass('btn-danger');
                        console.log(response);
                    });
            }
        });

        //
        // Paginação Ajax
        //
        $(document).on("click", ".pagination a, .paginate_button, .sort-group .dropdown-menu a", function (event) {
            event.preventDefault();

            $.ajax({
                url         : $(this).attr('href'),
                dataType    : "html",
                evalScripts : true,
                success:function (data, textStatus) {
                    $("#PageContent").html(data);
                    Echo.init();
                }
            });
        });

    });

    // LazyLoad images
    Echo.init({
        offset   : '0',
        throttle : '250'
    });

    // Transform alert messages in pnotify alerts
    utils.consume_alert();
});
