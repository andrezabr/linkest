/*!
 * Bootstrap v3.1.1 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
define(function(require) {
    'use strict';

    var $ = require('jquery');
            require('affix');
            require('alert');
            require('button');
            require('carousel');
            require('collapse');
            require('dropdown');
            require('modal');
            require('popover');
            require('scrollspy');
            require('tab');
            require('tooltip');
            require('transition');
});