<?php
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        // 'Acl',
        /*'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email')
                )
            ),
            'loginAction'    => array('controller' => 'users', 'action' => 'login', ),
            'loginRedirect'  => array('controller' => 'roles', 'action' => 'index' ),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', ),
            'authError'     => 'É necessário a autenticação no sistema para acessar este conteúdo',
            'loginError'    => 'Erro na autenticação',
            'autoRedirect'  => false,
            'flash'         => array(
                'element' => 'alert',
                'key'     => 'auth',
                'params'  => array(
                    'class'  => 'alert-danger danger'
                )
            )
        ),*/
        // 'Grant',
        // 'Json',
        'RequestHandler',
        'Session',
        'Cookie',
        // 'DebugKit.Toolbar'
    );
    public $helpers = array(
        'Session',
        'Time',
        'Text',
        'Cache',
        'Html',
        'Form',
        'Paginator'
    );

    public function beforeFilter(){
        // pr(phpinfo());exit;
    }
}
