<?php
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::uses('String', 'Utility');
App::uses('CakeTime', 'Utility');
/**
* Main class for all app-wide utility methods
*
* @author Gabriel Lau <PoloID>
* @license MIT
*/
class Util{

    /**
     * Retorna o IP do usuário
     * @param boolean $ip2long
     * @param boolean $safe
     * @return int | string
     * @link http://stackoverflow.com/questions/15699101/get-the-client-ip-address-using-php
     */
    public static function clientIp($ip2long = false, $safe = true){
        $ip = CakeRequest::clientIp($safe);

        # The $ip would now look something like: 1073732954
        if($ip2long)
            $ip = ip2long($ip);

        return $ip;
    }
    public static function clientId($ip2long = false){
        return PoloUtility::clientIp($ip2long) . gethostname();
    }

    /**
     * Padrão de resposta em formato json
     * @param  string $message [mensagem de resposta]
     * @param  string $status  [sinaliza a resposta como bem sucedida ou falha]
     * @param  array  $fields  [campos adicionais de resposta no formato: titulo_da_resposta => valor_da_resposta]
     * @return jsonString
     */
    public function jsonResponse($message = '', $status = 'success', $fields = array()){
        return json_encode(Hash::merge(array(
            'status'  => $status,
            'message' => $message
        ), $fields));
    }
    public function jsonSuccess($message = '', $fields = array()){
        return self::jsonResponse($message, 'success', $fields);
    }
    public function jsonFail($message = '', $fields = array()){
        return self::jsonResponse($message, 'fail', $fields);
    }

    /*
    ** Transfere o arquivo para o destino final
    **
    ** @var array $imgtmp: array com as imagens
    ** @var string $tipo: tipo da galeria a ser cadastrada (galeria, obra, planta)
    ** @return boolean : Se cadastrou ou nao
    */
    public static function moveFile($file, $newFile, $file_default = null, $para = '', $de = ''){
        if(empty($de)) $de = UPLOAD_DIR.'/tmp/';
        if(empty($para)) $para = UPLOAD_DIR.'/';

        if(!is_file($de.$file)){
            CakeLog::write("debug", "Não foi possível transferir a imagem: ".$de.$file);
            // throw new GoodsException('Não foi possível transferir a imagem: $file.');
            return false;
        }
        /*
        ** Cria os diretórios e subdiretórios se não existirem
        */
        if(!is_dir($para)) {
            $folder = new Folder();
            if(!$folder->create($para,0777)) {
                CakeLog::write("debug","Não deu pra criar o diretório: ".$para);
        // die("Não deu pra criar o diretório: ".$para);
                return false;
            }
        }
        // else CakeLog::write("debug","Criou o diretório: ".$para);

        //Transfere o arquivo para o diretorio final
        if(!rename($de.$file, $para.$newFile)) {
            CakeLog::write("debug","Houve um erro ao renomear o arquivo.");
        // die("Houve um erro ao renomear o arquivo.");
        // throw new GoodsException("Houve um erro ao renomear o arquivo.");
            return false;
        }
        //Apaga o arquivo antigo (se tiver)
        if(!empty($file_default)){
            self::deleteFile($para.$file_default);
        }

        return true;
    }//end action moveFile()

    /**
    * Apaga um arquivo do sistema
    *
    * @var string $filepath: caminho até o arquivo
    * @return boolean
    */
    public static function deleteFile($filepath=null){
        if(empty($filepath)) return false;

        if(is_file($filepath)){
            $file = new File($filepath);
            if(!$file->delete()) return false;
            $file->close();
        }
        else{
            CakeLog::write("debug","Não foi possível achar o arquivo em: ".$filepath);
        }
        return true;
    }

    public static function deleteDir($filepath=null){
        if(empty($filepath)) return false;

        if(is_dir($filepath)){
            $folder = new Folder($filepath);
            if(!$folder->delete()) return false;
        }
        else{
            CakeLog::write("debug","Não foi possível achar o diretório em: ".$filepath);
        }
        return true;
    }

    public static function css_browser_selector($ua=null) {
        $ua = ($ua) ? strtolower($ua) : strtolower($_SERVER['HTTP_USER_AGENT']);

        $g = 'gecko';
        $w = 'webkit';
        $s = 'safari';
        $b = array();

        // browser
        if(!preg_match('/opera|webtv/i', $ua) && preg_match('/msie\s(\d)/', $ua, $array)) {
            $b[] = 'ie ie' . $array[1];
        } else if(strstr($ua, 'firefox/2')) {
            $b[] = $g . ' ff2';
        } else if(strstr($ua, 'firefox/3.5')) {
            $b[] = $g . ' ff3 ff3_5';
        } else if(strstr($ua, 'firefox/3')) {
            $b[] = $g . ' ff3';
        } else if(strstr($ua, 'gecko/')) {
            $b[] = $g;
        } else if(preg_match('/opera(\s|\/)(\d+)/', $ua, $array)) {
            $b[] = 'opera opera' . $array[2];
        } else if(strstr($ua, 'konqueror')) {
            $b[] = 'konqueror';
        } else if(strstr($ua, 'chrome')) {
            $b[] = $w . ' ' . $s . ' chrome';
        } else if(strstr($ua, 'iron')) {
            $b[] = $w . ' ' . $s . ' iron';
        } else if(strstr($ua, 'applewebkit/')) {
            $b[] = (preg_match('/version\/(\d+)/i', $ua, $array)) ? $w . ' ' . $s . ' ' . $s . $array[1] : $w . ' ' . $s;
        } else if(strstr($ua, 'mozilla/')) {
            $b[] = $g;
        }

        // platform
        if(strstr($ua, 'j2me')) {
            $b[] = 'mobile';
        } else if(strstr($ua, 'iphone')) {
            $b[] = 'iphone';
        } else if(strstr($ua, 'ipod')) {
            $b[] = 'ipod';
        } else if(strstr($ua, 'mac')) {
            $b[] = 'mac';
        } else if(strstr($ua, 'darwin')) {
            $b[] = 'mac';
        } else if(strstr($ua, 'webtv')) {
            $b[] = 'webtv';
        } else if(strstr($ua, 'win')) {
            $b[] = 'win';
        } else if(strstr($ua, 'freebsd')) {
            $b[] = 'freebsd';
        } else if(strstr($ua, 'x11') || strstr($ua, 'linux')) {
            $b[] = 'linux';
        }

        return join(' ', $b);
    }




  /**
  * Função para gerar senhas aleatórias
  *
  * @author    Thiago Belem <contato@thiagobelem.net>
  *
  * @param integer $tamanho Tamanho da senha a ser gerada
  * @param boolean $maiusculas Se terá letras maiúsculas
  * @param boolean $numeros Se terá números
  * @param boolean $simbolos Se terá símbolos
  *
  * @return string A senha gerada
  */

    // public $lmin = 'abcdefghijklmnopqrstuvwxyz';
    // public $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    // public $num  = '1234567890';
    // public $simb = '!@#$%*-';


    public static function geraSenha($tamanho = 8, $minusculas = true, $maiusculas = true, $numeros = true, $simbolos = false){
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num  = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        if ($minusculas) $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros)    $caracteres .= $num;
        if ($simbolos)   $caracteres .= $simb;

        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand     = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;
    }

/**
 * Função para remover acentos de uma string
 *
 * @author Thiago Belem <contato@thiagobelem.net>
 */
    public static function slug($string, $slug = '-') {
        $string = strtolower($string);

        // Código ASCII das vogais
        $ascii['a'] = range(224, 230);
        $ascii['e'] = range(232, 235);
        $ascii['i'] = range(236, 239);
        $ascii['o'] = array_merge(range(242, 246), array(240, 248));
        $ascii['u'] = range(249, 252);

        // Código ASCII dos outros caracteres
        $ascii['b'] = array(223);
        $ascii['c'] = array(231);
        $ascii['d'] = array(208);
        $ascii['n'] = array(241);
        $ascii['y'] = array(253, 255);

        foreach ($ascii as $key=>$item) {
            $acentos = '';
            foreach ($item AS $codigo) $acentos .= chr($codigo);
            $troca[$key] = '/['.$acentos.']/i';
        }

        $string = preg_replace(array_values($troca), array_keys($troca), $string);

        // Slug?
        if ($slug) {
          // Troca tudo que não for letra ou número por um caractere ($slug)
            $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
          // Tira os caracteres ($slug) repetidos
            $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
            $string = trim($string, $slug);
        }

        return $string;
    }

/**
 * Função para tratar uma string para URL seguindo padrões SEO
 *
 * @author Gabriel Lau <@okatsuralau>
 * @uses String.Utility
 */
    public static function urlseo($string = null){
        if( !empty($string) ){
            return strtolower(Inflector::slug( String::truncate(
                $string,
                60,
                array(
                    'ending' => '',
                    'exact'  => false
                    )
                )
            ));
        }
    }


/**
 * Funçao que retorna o nome do mes por extenso
 * @param  string/int $m [número do mês]
 * @return string    [iniciais do mês]
 */
    public static function getMesAbr($m){
        switch($m){
            case "01": return "Jan"; break;
            case "02": return "Fev"; break;
            case "03": return "Mar"; break;
            case "04": return "Abr"; break;
            case "05": return "Mai"; break;
            case "06": return "Jun"; break;
            case "07": return "Jul"; break;
            case "08": return "Ago"; break;
            case "09": return "Set"; break;
            case "10": return "Out"; break;
            case "11": return "Nov"; break;
            case "12": return "Dez"; break;
        }
        return $m;
    }

    /*
    ** Funções auxiliares para tratamento do path
    */
    public function fixDoubleSlash($path){
        $path = str_replace("\\\\", "\\", $path);
        $path = str_replace("//", "/", $path);
        return $path;
    }
    public function pathSlash($path){
        $path = str_replace("\\", "/", $path);
        return $path;
    }
    public function pathBackSlash($path){
        $path = str_replace("/", "\\", $path);
        return $path;
    }




    #
    # Date Utilities
    #
    public function formatDate($date='', $format='dmy', $separator=null){
        $out = '';
        if(!empty($date)):
            switch ($format){
                case 'timeago':
                    $out = CakeTime::timeAgoInWords( $date, array('format' => 'F jS, Y', 'accuracy' => array('month' => 'month'),'end' => '1 year'));
                    break;
                case 'dmat':
                    list($dia, $mes, $ano, $time) = explode(' ', CakeTime::format('j m Y H:i', $date));
                    $out = __( "%s/%s/%s, %s", $dia, __($mes), $ano, $time );
                    break;
                case 'dmy':
                    if(is_null($separator)) $separator = "/";
                    list($dia, $mes, $ano) = explode(' ', CakeTime::format('d m Y', $date));
                    $out = __( "%s$separator%s$separator%s", $dia, __($mes), $ano );
                    break;
                case 'dFY':
                    if(is_null($separator)) $separator = ' de ';
                    list($dia, $mes, $ano) = explode(' ', CakeTime::format('d F Y', $date));
                    $out = __( "%s$separator%s$separator%s", $dia, __($mes), $ano );
                    break;
                default:
                    $out = CakeTime::format($format, $date);
                    break;
            }
        endif;
        return $out;
    }


    public function hashUrlKeys($params){
        return implode('/', array_filter(array_map(function ($v, $k) { return !is_bool($v) ? $k . ':' . $v : ''; }, $params, array_keys($params))));
    }

    // Utils
    // @link http://stackoverflow.com/questions/834303/php-startswith-and-endswith-functions
    function startsWith($haystack, $needle){
        return !strncmp($haystack, $needle, strlen($needle));
    }
    function endsWith($haystack, $needle){
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}
