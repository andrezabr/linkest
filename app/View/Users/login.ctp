<?php
	$this->assign('title', 'Identificação');

	$_ModelName = Inflector::classify( $this->params['controller']);
	echo $this->Form->create($_ModelName, array(
		'url' => array('controller' => 'users', 'action' => 'login', 'admin'=>false, 'plugin'=>PoloCMS),
        'inputDefaults' => array(
			'label'     => array('class'=>'sr-only'),
			'div'       => array('class' => 'form-group clearfix'),
			'wrapInput' => 'input-group',
			'class'     => 'form-control input-lg',
            'required'
        ),
	));
	echo '<h3 class="icomoon icon-locked f-thin color-white"> Identificação</h3>';
	echo '<section class="panel">';
		echo '<div class="panel-body">';
			echo $this->Form->input('email', array(
				'placeholder' => 'Email',
				'beforeInput' => '<span class="input-group-addon"><i class="icomoon icon-lg icon-envelope"></i></span>'
			));

			echo $this->Form->input('password', array(
				'placeholder' => 'Senha',
				'type'        => 'password',
				'beforeInput' => '<span class="input-group-addon"><i class="icomoon icon-lg icon-key"></i></span>'.$this->Html->link('<span class="sr-only">Precisa de ajuda com a sua senha?</span><i class="icon-help icon-lg"></i>', array('controller'=>'users','action'=>'forgot_password', 'admin'=>false), array('class'=>'forgot_password_link', 'title'=>'Precisa de ajuda com a sua senha?', 'escape'=>false, 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'tabindex'=>'-1'))
			));
		echo '</div>';
	echo '</section>';


	# Submit
	echo $this->Form->submit('Acessar', array('class'=>'btn btn-primary btn-lg btn-block'));

	# RememberMe
	echo $this->Form->switchbutton('remember_me', array(
		'label' => array(
			'text'  =>'Manter-me conectado',
			'class' => 'control-label col-md-6 col-sm-6 col-xs-6 f-thin color-white'
		),
		'required'  => false,
		'checked'   => true,
		'div'       => array('class' => 'form-group row mt15'),
		'between'   => '<div class="col-md-6 col-sm-6 col-xs-6 text-right">',
		'wrapInput' => array('data-on' => 'primary' )
	));




	// echo '<br>'.Security::hash('abc', 'sha1', true);
	// TODO: forgot_password????


	echo $this->Form->end();
?>