<?php
    // TODO: coletar mais informações dinamicamente
?>
<article class="panel panel-default">
    <div class="post-info">
        <time>38m ago</time>

        <span class="tags">
            <i class="glyphicon glyphicon-tag"></i>
            <a href="#">Java</a>
        </span>
    </div>
    <figure>
    <a href="#"><img src="http://dummyimage.com/16:9x150/f5f8fa/a3acb2"  class="img-responsive" /></a>
    </figure>

    <?php
        echo __('<h1>%s</h1>', $this->Html->link(h($link['Link']['url']), $link['Link']['url'], array('target'=>'_blank')));
    ?>
    <!-- <h1><a href="#">Jelly-o sweet jujubes lemon drops gummies</a></h1> -->

    <div class="btn-toolbar" role="toolbar">
        <div class="btn-group">
            <button type="button" class="actionLike btn btn-sm btn-success" title="Gostei">
                <i class="glyphicon glyphicon-thumbs-up"></i>
                <span class="sr-only">Gostei</span>
            </button>
            <button type="button" class="actionDislike btn btn-sm btn-default" title="Não gostei">
                <i class="glyphicon glyphicon-thumbs-down"></i>
                <span class="sr-only">Não gostei</span>
            </button>
        </div>

        <div class="btn-group">
            <?php
                echo $this->Html->link( '<i class="glyphicon glyphicon-heart"></i><span class="sr-only">Favoritar</span>', array('action' => 'edit', $link['Link']['id']), array('class'=>'actionFavorite btn btn-sm btn-default', 'title' => 'Favoritar', 'escape'=>false));
            ?>
        </div>
        <!-- <div class="btn-group">
            <button type="button" class="actionFavorite btn btn-sm btn-default" title="Favoritar">
                <i class="glyphicon glyphicon-heart"></i>
                <span class="sr-only">Favoritar</span>
            </button>
        </div> -->

            <!-- <button type="button" class="actionDelete btn btn-sm btn-danger" title="Excluir">
                <i class="glyphicon glyphicon-trash"></i>
                <span class="sr-only">Excluir</span>
            </button> -->
        <div class="btn-group">
            <?php
                echo $this->Form->postLink( '<i class="glyphicon glyphicon-trash"></i><span class="sr-only">Excluir</span>', array('action' => 'delete', $link['Link']['id']), array('class'=>'actionDelete btn btn-sm btn-danger', 'title' => 'Excluir', 'escape'=>false), __('Tem cetereza que quer excluir # %s?', $link['Link']['id']));
            ?>
            <button type="button" class="actionFlag btn btn-sm btn-default" title="Denunciar">
                <i class="glyphicon glyphicon-flag"></i>
                <span class="sr-only">Denunciar</span>
            </button>
        </div>

        <div class="btn-group">
            <button type="button" class="actionShare btn btn-sm btn-default" title="Compartilhar">
                <i class=" glyphicon glyphicon-send"></i>
                <span class="sr-only">Compartilhar</span>
            </button>
        </div>
    </div>
</article>