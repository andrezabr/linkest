<nav id="NavHeader" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<?php echo $this->Html->url('/')?>"><i class="glyphicon glyphicon-home"></i> <span class="hidden-xs hidden-sm"> Início</span></a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-heart"></i> <span class="hidden-xs hidden-sm"> Favoritos</span></a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-tags"></i> <span class="hidden-xs hidden-sm"> Categorias</span></a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i> <span class="hidden-xs hidden-sm"> Perfil</span></a></li>
                </ul>
            </div>

            <div class="col-sm-2 text-center">
                <a id="Logo" href="#">
                    <?php
                        echo $this->Html->image('logo-linkest-down-02-orange.png', array('height' => 50));
                    ?>
                    <!-- <img src="img/logo-linkest-down-02-orange.png" height="50" /> -->
                    <span class="sr-only">Linkest</span>
                </a>
            </div>

            <div class="col-sm-5">
                <?php
                    echo $this->Html->link('<i class="glyphicon glyphicon-edit"></i> <span class="sr-only">Publicar</span>', array('controller'=>'links', 'action' => 'add'), array('class' => 'btn btn-primary navbar-btn pull-right', 'escape' => false));
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-cog"></i> <span class="sr-only">Configurações</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Editar meu perfil</a>
                            </li>
                            <li><a href="#">Preferências</a></li>
                            <li><a href="#">Configurações</a></li>
                            <li><a href="#">Sair</a></li>
                        </ul>
                    </li>
                    <!-- <li>
                        <button type="submit" class="btn btn-primary navbar-btn"><i class="glyphicon glyphicon-edit"></i> <span class="sr-only">Publicar</span></button>
                    </li> -->
                </ul>

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Filtrar">
                    </div>
                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> <span class="sr-only">Pesquisar</span></button>
                </form>
            </div>
        </div>
    </div>
</nav>