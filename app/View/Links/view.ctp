<div class="links view">
<h2><?php echo __('Link'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($link['Link']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($link['User']['id'], array('controller' => 'users', 'action' => 'view', $link['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($link['Link']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip'); ?></dt>
		<dd>
			<?php echo h($link['Link']['ip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Machine'); ?></dt>
		<dd>
			<?php echo h($link['Link']['machine']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Link'), array('action' => 'edit', $link['Link']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Link'), array('action' => 'delete', $link['Link']['id']), null, __('Are you sure you want to delete # %s?', $link['Link']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Links'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Link'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Favorites'), array('controller' => 'favorites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Favorite'), array('controller' => 'favorites', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flags'), array('controller' => 'flags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Link Categories'), array('controller' => 'link_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Link Category'), array('controller' => 'link_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reviews'), array('controller' => 'reviews', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Review'), array('controller' => 'reviews', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Favorites'); ?></h3>
	<?php if (!empty($link['Favorite'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Link Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($link['Favorite'] as $favorite): ?>
		<tr>
			<td><?php echo $favorite['id']; ?></td>
			<td><?php echo $favorite['link_id']; ?></td>
			<td><?php echo $favorite['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'favorites', 'action' => 'view', $favorite['link_id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'favorites', 'action' => 'edit', $favorite['link_id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'favorites', 'action' => 'delete', $favorite['link_id']), null, __('Are you sure you want to delete # %s?', $favorite['link_id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Favorite'), array('controller' => 'favorites', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Flags'); ?></h3>
	<?php if (!empty($link['Flag'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Link Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Ip'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($link['Flag'] as $flag): ?>
		<tr>
			<td><?php echo $flag['id']; ?></td>
			<td><?php echo $flag['user_id']; ?></td>
			<td><?php echo $flag['link_id']; ?></td>
			<td><?php echo $flag['created']; ?></td>
			<td><?php echo $flag['ip']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'flags', 'action' => 'view', $flag['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'flags', 'action' => 'edit', $flag['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'flags', 'action' => 'delete', $flag['id']), null, __('Are you sure you want to delete # %s?', $flag['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Link Categories'); ?></h3>
	<?php if (!empty($link['LinkCategory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Link Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($link['LinkCategory'] as $linkCategory): ?>
		<tr>
			<td><?php echo $linkCategory['id']; ?></td>
			<td><?php echo $linkCategory['category_id']; ?></td>
			<td><?php echo $linkCategory['link_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'link_categories', 'action' => 'view', $linkCategory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'link_categories', 'action' => 'edit', $linkCategory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'link_categories', 'action' => 'delete', $linkCategory['id']), null, __('Are you sure you want to delete # %s?', $linkCategory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Link Category'), array('controller' => 'link_categories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Reviews'); ?></h3>
	<?php if (!empty($link['Review'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Link Id'); ?></th>
		<th><?php echo __('R Like'); ?></th>
		<th><?php echo __('R Dislike'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($link['Review'] as $review): ?>
		<tr>
			<td><?php echo $review['id']; ?></td>
			<td><?php echo $review['user_id']; ?></td>
			<td><?php echo $review['link_id']; ?></td>
			<td><?php echo $review['r_like']; ?></td>
			<td><?php echo $review['r_dislike']; ?></td>
			<td><?php echo $review['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'reviews', 'action' => 'view', $review['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'reviews', 'action' => 'edit', $review['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'reviews', 'action' => 'delete', $review['id']), null, __('Are you sure you want to delete # %s?', $review['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Review'), array('controller' => 'reviews', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
