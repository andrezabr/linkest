Linkest
====

PDS de sistemas distribuídos (2014.1).
[[Colocar descrição mais detalhada]]


Guia de Instalação para os desenvolvedores
----

- Faça um clone do projeto em um repositório local (dentro de seu diretório web):
```
$ git clone git@bitbucket.org:okatsuralau/linkest.git
```

> **OBS.:**
>
> O repositório contém **apenas** os arquivos referentes à aplicação. Será necessário fazer o download da biblioteca do Cake e adicioná-la no diretório do projeto.
>
> Para isso, baixe o arquivo https://github.com/cakephp/cakephp/zipball/2.5.1 e ponha a pasta lib ao lado da pasta app.
>
> A estrutura final, deverá ficar assim: **linkest/app** e **linkest/lib**.

- Crie os arquivos core.php e database.php utilizando os modelos existentes no diretório **app/Config** da aplicação principal;
- Crie um banco de dados em seu SqlServer local utilizando o modelo da aplicação (localizado em **app/Config/Schema/linkest - [versão].sql**);
- Pronto! Se o seu ambiente estiver configurado corretamente, a aplicação já estará funcionando. Acesse http://localhost/linkest para vê-lo em ação.


### Preparando o repositório local


- Cada desenvolvedor do projeto tem o seu próprio branch de trabalho chamado **[NomeDoSujeito]-master**. Neste branch, devem ser mescladas todas as alterações definitivas referentes ao projeto.

> Por segurança, é recomendado trabalhar em um outro branch e, após estar tudo corrigido, mesclar com o branch master pessoal.
>
> É importante que o desenvolvedor manipule apenas estes branchs e deixe que o(s) gerente(s) do projeto se encarreguem de mesclar as alterações com o branch master do projeto.

- Após ter feito o clone do repositório do projeto, faça o checkout para o seu branch pessoal (já criado pelo gerente):

```
$ cd linkest
$ git branch // lista os branchs existentes em seu repositorio
$ git branch -a // lista TODOS os branchs (locais e remotos)
$ git checkout origin/sujeito-dev // ambiente de desenvolvimento do projeto
$ git checkout origin/sujeito-master // ambiente de entrega das alterações
```

### Enviando modificações

- Faça algumas modificações e commite as mudanças (é importante que os commits sejam feitos logo após terminar as tarefas para não correr o risco de esquecer o que foi alterado posteriormente);

> Lembre-se que você deve estar no branch **dev** para fazer os commits

- Alterne para o branch master e mescle as modificações
- adicione (ao índice) os arquivos modificados usando
```
$ git add nome_do_arquivo
```

```
// commit as alterações
$ git checkout sujeito-master
$ git merge sujeito-dev
$ git checkout sujeito-dev // lembre-se de sempre voltar para o branch de desenvolvimento antes de iniciar uma nova modificação
$ git push origin sujeito-master // envia as modificações para o repositório remoto no Bitbucket
```
- Pronto! agora você já é um mestre do versionamento e capaz de gerenciar o seu projeto **\o/**.


Dicas
--

- Antes de modificar um arquivo, lembre-se de atualizar o seu repositório para pegar as últimas alterações feitas no projeto.

```
$ git pull origin
// OU
$ git pull origin html // onde html é o nome do branch a atualizar
```

TODO
--

- Adicionar links para soluções de problemas conhecidos.
- Adicionar Guia para o administrador do projeto
- Adicionar Guia mais detalhado sobre a instalação do projeto (Tecnologias necessárias + links)