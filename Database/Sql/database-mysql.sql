SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `linkedus` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `linkedus` ;

-- -----------------------------------------------------
-- Table `linkedus`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'categorias de usuários';


-- -----------------------------------------------------
-- Table `linkedus`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_id` INT(11) NOT NULL,
  `username` VARCHAR(16) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(32) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_user_role1_idx` (`role_id` ASC),
  CONSTRAINT `fk_user_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `linkedus`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'usuários do sistema';


-- -----------------------------------------------------
-- Table `linkedus`.`link`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`link` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tipo do conteúdo';


-- -----------------------------------------------------
-- Table `linkedus`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`image` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tipo do conteúdo';


-- -----------------------------------------------------
-- Table `linkedus`.`video`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`video` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tipo do conteúdo';


-- -----------------------------------------------------
-- Table `linkedus`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`file` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tipo do conteúdo';


-- -----------------------------------------------------
-- Table `linkedus`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) NULL,
  `title` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `lft` INT(11) NULL,
  `rght` INT(11) NULL,
  `bookmark_count` INT(11) NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NULL,
  `is_editable` TINYINT(11) NULL DEFAULT 1,
  `deleted` TINYINT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
COMMENT = 'Categoria de classificação do conteúdo';


-- -----------------------------------------------------
-- Table `linkedus`.`bookmark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`bookmark` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `link_id` INT(11) NULL,
  `image_id` INT(11) NULL,
  `video_id` INT(11) NULL,
  `file_id` INT(11) NULL,
  `ip` INT(10) NOT NULL,
  `machine` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bookmark_link_idx` (`link_id` ASC),
  INDEX `fk_bookmark_image1_idx` (`image_id` ASC),
  INDEX `fk_bookmark_video1_idx` (`video_id` ASC),
  INDEX `fk_bookmark_file1_idx` (`file_id` ASC),
  INDEX `fk_bookmark_user1_idx` (`user_id` ASC),
  INDEX `fk_bookmark_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_bookmark_link`
    FOREIGN KEY (`link_id`)
    REFERENCES `linkedus`.`link` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_image1`
    FOREIGN KEY (`image_id`)
    REFERENCES `linkedus`.`image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_video1`
    FOREIGN KEY (`video_id`)
    REFERENCES `linkedus`.`video` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_file1`
    FOREIGN KEY (`file_id`)
    REFERENCES `linkedus`.`file` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `linkedus`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'postagens do sistema (links)';


-- -----------------------------------------------------
-- Table `linkedus`.`tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`tag` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `slug` VARCHAR(45) NOT NULL,
  `bookmark_count` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Lista de palavras-chave do link';


-- -----------------------------------------------------
-- Table `linkedus`.`bookmark_tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`bookmark_tag` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `bookmark_id` INT(11) NOT NULL,
  `tag_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `bookmark_id`, `tag_id`),
  INDEX `fk_bookmark_has_tag_tag1_idx` (`tag_id` ASC),
  INDEX `fk_bookmark_has_tag_bookmark1_idx` (`bookmark_id` ASC),
  CONSTRAINT `fk_bookmark_has_tag_bookmark1`
    FOREIGN KEY (`bookmark_id`)
    REFERENCES `linkedus`.`bookmark` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_has_tag_tag1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `linkedus`.`tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `linkedus`.`like`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`like` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `bookmark_id` INT(11) NOT NULL,
  `like` TINYINT(1) NULL DEFAULT 0,
  `dislike` TINYINT(1) NULL DEFAULT 0,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_like_user1_idx` (`user_id` ASC),
  INDEX `fk_like_bookmark1_idx` (`bookmark_id` ASC),
  CONSTRAINT `fk_like_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_like_bookmark1`
    FOREIGN KEY (`bookmark_id`)
    REFERENCES `linkedus`.`bookmark` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'lista de links avaliados (Y/N)';


-- -----------------------------------------------------
-- Table `linkedus`.`favorite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`favorite` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `bookmark_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_favorite_user1_idx` (`user_id` ASC),
  INDEX `fk_favorite_bookmark1_idx` (`bookmark_id` ASC),
  CONSTRAINT `fk_favorite_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorite_bookmark1`
    FOREIGN KEY (`bookmark_id`)
    REFERENCES `linkedus`.`bookmark` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Links \"especiais\" para o usuário';


-- -----------------------------------------------------
-- Table `linkedus`.`request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`request` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_request_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_request_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Lista de links \"desejados\" pelos usuários';


-- -----------------------------------------------------
-- Table `linkedus`.`watch`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`watch` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_watch_user1_idx` (`user_id` ASC),
  INDEX `fk_watch_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_watch_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_watch_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `linkedus`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Lista de categorias de links que o usuário deseja \"observar\"';


-- -----------------------------------------------------
-- Table `linkedus`.`flag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `linkedus`.`flag` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `bookmark_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL COMMENT 'Usuário que sinalizou o link',
  `created` DATETIME NOT NULL,
  `ip` INT(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flag_bookmark1_idx` (`bookmark_id` ASC),
  INDEX `fk_flag_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_flag_bookmark1`
    FOREIGN KEY (`bookmark_id`)
    REFERENCES `linkedus`.`bookmark` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_flag_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `linkedus`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Links sinalizados como impróprios';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
